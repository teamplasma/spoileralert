//
//  NewSpoil.swift
//  spoiler
//
//  Created by Vincent Lepigeon on 06/12/2016.
//  Copyright © 2016 Vincent. All rights reserved.
//

import Foundation
import UIKit
import Realm
import RealmSwift
import Contacts
import EventKit

class NewSpoil: UIViewController {
    
    @IBOutlet weak var destinataire: UITextField!
    @IBOutlet weak var content: UITextField!
    
    @IBOutlet weak var destError: UILabel!
    

    @IBOutlet weak var contentError: UILabel!
    
    @IBOutlet weak var calendar: UIDatePicker!
    
    public var num : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Changement de la couleur navigation bar
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        
        calendar.setValue(UIColor.white, forKey: "textColor")

       
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender : Any?){
        
        let DestViewController : SaveSpoil = segue.destination as! SaveSpoil
        
        DestViewController.dest = destinataire.text!
        DestViewController.content = content.text!
        
        
    }
    
    func addInCalendar(){
        
        let eventStore = EKEventStore()
        let newEvent = EKEvent(eventStore: eventStore)
        
        //5 min de plus que startDate
        let endDate = calendar.date.addingTimeInterval(5 * 60)
        
        //let newCalendar = EKCalendar()
        newEvent.calendar = eventStore.defaultCalendarForNewEvents
        newEvent.title = "Its time to spoil"
        newEvent.startDate = calendar.date
        newEvent.endDate = endDate
        
        print(newEvent)
        
        do{
            try eventStore.save(newEvent, span: .thisEvent, commit : true)
        } catch {
            print("Impossible de créer dans l'agenda ")
            
        }

    }
    
    @IBAction func save(_ sender: AnyObject) {
        
      
    
        if(destinataire.text == ""){
            destError.text = "Veuillez entrer un destinataire"
            if(content.text == "" ){
                contentError.text = "Veuillez entrer un contenu ou une photo"
            } else {
                contentError.text = ""
            }
        } else if(content.text == ""){
            destError.text = ""
            contentError.text = "Veuillez entrer un contenu ou une photo"
        } else {
        destError.text = ""
        contentError.text = ""
            
            let eventStore = EKEventStore()
            
            // Do any additional setup after loading the view, typically from a nib.
            if(EKEventStore.authorizationStatus(for: .event) != EKAuthorizationStatus.authorized){
                eventStore.requestAccess(to: .event, completion: { (granted, error) in
                    self.addInCalendar()
                })
            } else {
               self.addInCalendar()
            }
            
            
            
        let realm = try! Realm()
        try! realm.write(){
            //let spoil = Spoil(destinataire.text!, content.text!)
            let spoil = Spoil(NSUUID().uuidString)
            spoil.destinataire = destinataire.text!
            spoil.contenu = content.text!
            realm.add(spoil, update: true)
            print(realm.objects(Spoil.self))
        }
        num = num+1
        
        destinataire.text = ""
        content.text = ""
        destinataire.resignFirstResponder()
        content.resignFirstResponder()
        }
    }
    
    //Fermer clavier si clique n'importe ou
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
}
