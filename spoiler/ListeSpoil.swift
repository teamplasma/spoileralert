//
//  ListeSpoil.swift
//  spoiler
//
//  Created by Vincent Lepigeon on 09/12/2016.
//  Copyright © 2016 Vincent. All rights reserved.
//

import Foundation

import UIKit
import RealmSwift
import Realm

class ListeSpoil: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    var spoil : Spoil!
   var listeSpoil: Results<Spoil>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Changement de la couleur navigation bar
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        print("load on back")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Recharge les données de la tableView quand la page est appelé 
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let realm = try! Realm()
        let spoils = realm.objects(Spoil.self)
        return spoils.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell();
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cellule", for: indexPath)
        let realm = try! Realm()
        
        // Couleur du fond
        cell.backgroundColor = UIColor(red: 28/255, green: 32/255, blue: 57/255, alpha: 1.0)
        // couleur du texte
        cell.textLabel?.textColor = UIColor.white
        // désactive l'effet de survol
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        listeSpoil = realm.objects(Spoil.self)
        print("spiler")
        print(listeSpoil)
            cell.textLabel?.text = listeSpoil[indexPath.row].destinataire
            
        return cell
    }
    
    func numberOfSections(in tableView : UITableView )-> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let realm = try! Realm()
       // let triSerie = realm.objects(Serie.self).filter("nom BEGINSWITH '\(alphabet[indexPath.section])'")
        //spoil = triSerie[indexPath.row]
        let listeSpoil = realm.objects(Spoil.self)
        print(listeSpoil)
        print(listeSpoil[indexPath.row])
       // print(spoil)
        spoil = listeSpoil[indexPath.row]
        performSegue(withIdentifier: "showSpoil", sender: self)
        //Deselectionne la ligne pour ne pas qu'elle soit afficher en cliqué au retour
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showSpoil"){
            let viewDestination = segue.destination as! spoil
            viewDestination.spoil = spoil
        }
    }

    
    
}
