//
//  spoil.swift
//  spoiler
//
//  Created by Vincent Lepigeon on 16/01/2017.
//  Copyright © 2017 Vincent. All rights reserved.
//

import Foundation
import UIKit
import Realm
import RealmSwift

class spoil : UIViewController {
    
    var spoil : Spoil?
    
    @IBOutlet weak var destinataire: UILabel!
    @IBOutlet weak var content: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        destinataire.text = spoil?.destinataire
        content.text = spoil?.contenu
        print(spoil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func deleteSpoil(_ sender: UIButton) {
        let realm = try! Realm()
       try! realm.write {
            //Supprimr le spoil
            realm.delete(spoil!)
        }
        //Retour à la vue précedente : listeSpoil
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
