//
//  ViewController2.swift
//  spoiler
//
//  Created by Vincent Lepigeon on 22/10/2016.
//  Copyright © 2016 Vincent. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class ViewController2: UIViewController {
    
    var serie: Serie!
    
    @IBOutlet weak var nom: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        nom.text = serie.nom
        content.text = serie.desc
        image.image = UIImage(named : serie.img)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Redirige sur la creation de spoil
    @IBAction func newSpoil(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 1
    }
    
}
