//
//  ViewController.swift
//  spoiler
//
//  Created by Vincent Lepigeon on 22/10/2016.
//  Copyright © 2016 Vincent. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import CoreSpotlight
import MobileCoreServices

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    
    var listeSerie : Results<Serie>!
    
    var movieDetails : Serie!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Changement de la couleur navigation bar
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        
                
        let realm = try! Realm()
        print(realm.configuration.fileURL)
        try! realm.write(){
            
            var bad = Serie("Breaking Bad")
            bad.id = 1
            bad.img = "breakingbad"
            bad.desc = "Walter White est professeur de chimie dans un lycée et vit avec son fils handicapé et sa femme enceinte à Albuquerque, au Nouveau-Mexique. Lorsqu'on lui diagnostique un cancer du poumon en phase terminale, tout s'effondre. Il décide alors de mettre en place un laboratoire de méthamphétamine pour subvenir aux besoins de sa famille en s'associant avec l'un de ses anciens élèves, Jesse Pinkman, devenu petit trafiquant."
            realm.add(bad, update: true)
            
            var got = Serie("Games of Thrones")
            got.id = 2
            got.img = "got"
            got.desc = "L'histoire se déroule dans un univers fictif composé de deux continents : Westeros à l'Ouest et Essos à l'Est. La maison Stark, dirigée par Lord Eddard Stark, est impliquée dans les intrigues du pouvoir de la cour du Roi Robert Baratheon lorsque la Main du roi Jon Arryn (le conseiller principal de Robert), meurt mystérieusement."
            realm.add(got, update: true)
            
            var gorgia = Serie("Borgia")
            gorgia.id = 4
            gorgia.img = "borgias"
            gorgia.desc = "L’accession au pouvoir du cardinal espagnol Rodrigo Borgia (né à Xàtiva, Royaume de Valence, Espagne) et de son clan, qui s’efforcèrent d’instaurer une dynastie pour exercer leur domination sur le monde. Bien qu’étant un homme de foi, Rodrigue était aussi esclave des plaisirs charnels. Il devait non seulement déjouer les complots et les conspirations de ses collègues cardinaux et des représentants des grands pouvoirs, mais aussi mener une lutte pour contenir les rivalités qui menaçaient de déchirer sa famille."
            realm.add(gorgia, update: true)

            
            var once = Serie("Once upon a time")
            once.id = 3
            once.img = "once"
            once.desc = "Il était une fois, dans une forêt enchantée vivaient les personnages de contes que nous connaissons, ou que nous pensions connaître. Un jour, à cause d'une terrible malédiction, ils se retrouvèrent coincés dans un monde où toutes leurs fins heureuses leur furent enlevées, notre monde. Une seule personne peut rompre la malédiction : la fille de Blanche Neige et du Prince Charmant, produit d'un véritable amour."
            realm.add(once, update: true)
            
            var mrRobot = Serie("Mr.Robot")
            mrRobot.id = 5
            mrRobot.img = "mrrobot"
            mrRobot.desc = "Elliot Alderson est un jeune informaticien vivant à New York, qui travaille en tant qu'ingénieur en sécurité informatique pour Allsafe Security. Luttant constamment avec un trouble d’anxiété sociale et de dépression, le processus de pensée d’Elliot semble fortement influencé par la paranoïa et l'illusion. Il hacke les comptes des gens, ce qui le conduit souvent à agir comme un cyber-justicier. Elliot rencontre un mystérieux anarchiste connu sous le nom de « Mr. Robot » qui souhaite le recruter dans son groupe de hackers connu sous le nom de « Fsociety ». Leur objectif consiste à rétablir l'équilibre de la société en détruisant les infrastructures des plus grosses banques et entreprises du monde, notamment le conglomérat E Corp. (nommé « Evil Corp. » par Elliot), qui représente également 80 % du chiffre d’affaires d’Allsafe Security."
            realm.add(mrRobot, update: true)
            
            var lescent = Serie("Les 100")
            lescent.id = 6
            lescent.img = "les100"
            lescent.desc = "Après une apocalypse nucléaire causée par l'Homme lors d'une troisième Guerre Mondiale, les 318 survivants recensés se réfugient dans des stations spatiales et parviennent à y vivre et à se reproduire, atteignant le nombre de 4000. Mais 97 ans plus tard, le vaisseau mère, l'Arche, est en piteux état. Une centaine de jeunes délinquants emprisonnés au fil des années pour des crimes ou des trahisons sont choisis comme cobayes par les autorités pour redescendre sur Terre et tester les chances de survie. Dès leur arrivée, ils découvrent un nouveau monde dangereux mais fascinant..."
            realm.add(lescent, update: true)
            
            var walkingDead = Serie("The walking dead")
            walkingDead.id = 7
            walkingDead.img = "walkingdead"
            walkingDead.desc = "près une apocalypse ayant transformé la quasi-totalité de la population en zombies, un groupe d'hommes et de femmes mené par l'officier Rick Grimes tente de survivre... Ensemble, ils vont devoir tant bien que mal faire face à ce nouveau monde devenu méconnaissable, à travers leur périple dans le Sud profond des États-Unis."
            realm.add(walkingDead, update: true)
            
            var vikings = Serie("Vikings")
            vikings.id = 8
            vikings.img = "vikings"
            vikings.desc = "Scandinavie, à la fin du 8ème siècle. Ragnar Lodbrok, un jeune guerrier viking, est avide d'aventures et de nouvelles conquêtes. Lassé des pillages sur les terres de l'Est, il se met en tête d'explorer l'Ouest par la mer. Malgré la réprobation de son chef, Haraldson, il se fie aux signes et à la volonté des dieux, en construisant une nouvelle génération de vaisseaux, plus légers et plus rapides..."
            realm.add(vikings, update: true)
        }

        print(realm.objects(Serie.self))
        listeSerie = realm.objects(Serie.self)
        
        for i in 0...(listeSerie.count - 1) {
        
            // Create an attribute set to describe an item.
            let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeData as String)
            // Add metadata that supplies details about the item.
            attributeSet.title = listeSerie[i].nom
            attributeSet.contentDescription = listeSerie[i].desc
            attributeSet.thumbnailData =  UIImagePNGRepresentation(UIImage(named: listeSerie[i].img)!)
        
            // Create an item with a unique identifier, a domain identifier, and the attribute set you created earlier.
            let item = CSSearchableItem(uniqueIdentifier: "\(i)", domainIdentifier: "file-1", attributeSet: attributeSet)
        
            // Add the item to the on-device index.
            CSSearchableIndex.default().indexSearchableItems([item]) { error in
                if error != nil {
                    print(error?.localizedDescription)
                }
                else {
                    print("Item indexed.")
                }
            }
        }

        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let realm = try! Realm()
        let nbSerie = realm.objects(Serie.self).filter("nom BEGINSWITH '\(alphabet[section])'")
        return nbSerie.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell();
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cellule", for: indexPath)
        let realm = try! Realm()

        let triSerie = realm.objects(Serie.self).filter("nom BEGINSWITH '\(alphabet[indexPath.section])'")
        print(triSerie)
        // Couleur du fond
        cell.backgroundColor = UIColor(red: 34/255, green: 39/255, blue: 70/255, alpha: 1.0)
        // couleur du texte
        cell.textLabel?.textColor = UIColor.white
        // désactive l'effet de survol
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        // Changement de type de font
        cell.textLabel?.font = UIFont(name: "OpenSans-Regular", size: 14)
        
        // Enleve la marge sur la gauche
        //tableView.contentInset = UIEdgeInsetsMake(0, -15, 0, 0)
        //cell.layoutMargins = UIEdgeInsets.zero
        
        //tableView.layoutMargins = UIEdgeInsets.zero
        //tableView.separatorInset = UIEdgeInsets.zero
        
        cell.textLabel?.text = triSerie[indexPath.row].nom
        cell.imageView?.image = UIImage(named : triSerie[indexPath.row].img)        
        
        
        
        return cell
    }
    
    func numberOfSections(in tableView : UITableView )-> Int {
        return alphabet.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return alphabet[section]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let realm = try! Realm()
        let triSerie = realm.objects(Serie.self).filter("nom BEGINSWITH '\(alphabet[indexPath.section])'")
        movieDetails = triSerie[indexPath.row]
        performSegue(withIdentifier: "idSegueShowMovieDetails", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "idSegueShowMovieDetails"){
            let viewDestination = segue.destination as! ViewController2
            viewDestination.serie = movieDetails
        }
    }


}

