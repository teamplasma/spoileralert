//
//  Serie.swift
//  Realm
//
//  Created by Vincent Lepigeon on 18/11/2016.
//  Copyright © 2016 Vincent. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class Serie: Object {
    
    dynamic var id = 0
    dynamic var nom = ""
    dynamic var desc = ""
    dynamic var img = ""
    
    convenience init( _ nom: String) {
        self.init()
        //id = NSUUID().uuidString
        self.nom = nom
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}


class Spoil: Object {
    
    dynamic var id = ""
    dynamic var destinataire = ""
    dynamic var contenu = ""
    
    convenience init(_ num: String) {
        self.init()
        self.id = num
        //NSUUID().uuidString
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
